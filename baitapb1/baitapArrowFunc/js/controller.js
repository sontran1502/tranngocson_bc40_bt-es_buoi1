export let renderButton = (color) => { 
    let buttonList = "";
    color.forEach((item) => { 
        let button = /*html*/ `
            <button   class="color-button ${item}"></button>         
         `
         buttonList+= button;
     })
     document.getElementById("colorContainer").innerHTML= buttonList;
 }