import { renderButton } from "./controller.js";

const color = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

renderButton(color);

const btns = document.querySelectorAll("button.color-button");

btns.forEach((btn) => {
    btn.addEventListener("click", () => {
        if (document.getElementById("house").classList.length > 1) {
            document.getElementById("house").className = "house";
        }
        let classHouse = btn.classList[1]
        document.getElementById("house").classList.add(classHouse);

    })
})

document.getElementsByClassName("color-button")[0].classList.add("active")

btns.forEach((btn)=>{
    btn.addEventListener("click", (e) => {
        var domAct = document.querySelector(".active");
        if (domAct != null) {
            domAct.classList.remove("active");
        }
        e.target.classList.add("active");
    });
})

